const awsIot = require('aws-iot-device-sdk')

const host = process.argv[2]
const stage = process.argv[3]
const clientId = process.argv[4]

/*
 Note: use stage and clientId to as variables to create unique topics as below
*/

const topic = `workshop/${stage}/${clientId}/mytopic`

start()

function start(profile) {
  console.log(`Host: ${host} | ClientId: ${clientId} | Topic: ${topic}`)

  const device = awsIot.device({
    host: host,
    keyPath: 'private.pem',
    certPath: 'cert.pem',
    caPath: 'ca.pem',
    clientId: clientId
  });

  device
    .on('connect', function() {
      console.log('connected');
    })
    .on('close', function() {
      console.log('closed');
    })
    .on('reconnect', function() {
      console.log('reconnecting');
    })
    .on('message', function(topic, payload) {
      console.log('message', topic, payload.toString());
    });

}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}
